/* -*- Mode: vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */
/* pimp-print.vala
 *
 * Copyright (C) 2017 Daniel Espinosa <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;
using Qrencode;

public errordomain QRSvg.QRError {
  INVALID_ENCODE
}

/**
 * Main class to construct SVG representation of QR codes.
 */
public class QRSvg.QR : Object {
  private GSvg.Document _svg;
  private GElement top_g;
  private int _width;
  private double _svg_width;
  private int _version;
  private double _square_width = 1.0;

  /**
   * QR version number to use
   */
  public int version {
    get {
      return _version;
    }
    set {
      _version = value;
      try {
        prepare ();
      } catch (GLib.Error e) {
        warning ("Error while setting QR's version: %s", e.message);
      }
    }
  }
  /**
   * Number of units per side in square
   */
  public int width { get { return _width; } }
  /**
   * Width of square in milimeters
   */
  public double square_width {
    get { return _square_width; }
    set {
      _square_width = value;
      try {
        prepare ();
      } catch (GLib.Error e) {
        warning ("Error while setting square's width: %s", e.message);
      }
    }
  }
  /**
   * Width of QRcode in milimeters
   */
  public double svg_width { get { return _svg_width; } }
  /**
   * DPI in use
   */
  public double dpi { get; set; default=72; }
  /**
   * Generated SVG
   */
  public GSvg.Document svg { get { return _svg; } }
  construct {
    _svg = new GSvg.GsDocument ();
    _version = 1;
  }
  /**
   * Encodes a string rendering an SVG image based on it.
   *
   * @param str string to encode
   * @param quality Error detection quality
   * @param mode QR character mode
   * @param sensitive case sensitive
   */
  public void encode (string str, EcQuality quality, CharacterMode mode, CaseSensitive sensitive) throws GLib.Error
  {
    GSvg.LineElement line = null;
    var qr = init_qr (str, quality, mode, sensitive);
    int x, y;
    x = -1;
    y = 0;
    for (int i = 0; i < (qr.width * qr.width); i++) {
      x++;
      if (x == qr.width) {
        x = 0;
        y++;
        line = null;
      }
      uint8 px = qr.data[i];
      if (Data.ENABLE in px) {
        double posx = square_width*x;
        double posy = square_width*y+square_width/2;
        if (line == null) {
          line = add_line (posx, posy);
        } else {
          extend_line (line);
        }
      } else {
        line = null;
      }
    }
  }
  /**
   * Encodes asyncronically a string rendering an SVG image based on it.
   *
   * @param str string to encode
   * @param quality Error detection quality
   * @param mode QR character mode
   * @param sensitive case sensitive
   */
  public async void encode_async (string str, EcQuality quality, CharacterMode mode, CaseSensitive sensitive) throws GLib.Error
  {
    encode (str, quality, mode, sensitive);
  }
  private QRcode init_qr (string str, EcQuality quality,
                          CharacterMode mode,
                          CaseSensitive sensitive) throws GLib.Error
  {
    var qr = new QRcode.encodeString (str, version, (Qrencode.EcLevel) quality, (Qrencode.Mode) mode, sensitive);
    if (qr == null) {
      throw new QRError.INVALID_ENCODE ("String encode error: %s", error_to_string ());
    }
    _width = qr.width;
    prepare ();
    return qr;
  }
  private GSvg.LineElement add_line (double posx, double posy) throws GLib.Error {
    var l = svg.root_element.create_line ("%gmm".printf (posx),
                                          "%gmm".printf (posy),
                                          "%gmm".printf (posx + square_width),
                                          "%gmm".printf (posy),
                                          null
                                          );
    top_g.append_child (l);
    return l;
  }
  private void extend_line (GSvg.LineElement l) {
    l.x2.base_val.value += square_width;
  }
  private void prepare () throws GLib.Error {
    _svg_width = _square_width * _width;
    var strw = "%fmm".printf (_svg_width);
    if (svg.root_element != null) {
      var r = svg.root_element;
      if (r.x == null) {
        r.x = new GSvg.GsAnimatedLength ();
      }
      if (r.y == null) {
        r.y = new GSvg.GsAnimatedLength ();
      }
      if (r.width == null) {
        r.width = new GSvg.GsAnimatedLength ();
      }
      if (r.height == null) {
        r.height = new GSvg.GsAnimatedLength ();
      }
      r.x.value = "0mm";
      r.y.value = "0mm";
      r.width.value = strw;
      r.height.value = strw;
    } else {
      svg.add_svg ("0mm", "0mm", strw, strw, null);
    }
    if (top_g == null) {
      top_g = svg.root_element.add_g ();
      top_g.set_attribute ("style", "stroke: black; stroke-width: %gmm".printf (square_width*1.1));
    }
  }
  private string error_to_string () {
    switch (GLib.errno) {
      case Qrencode.ErrorCode.NO_MEM:
        return "unable to allocate memory for input objects";
      case Qrencode.ErrorCode.INVALID_VALUE:
        return "invalid input object";
      case Qrencode.ErrorCode.RANGE:
        return "input data is too large";
    }
    return "Unknown Error Code";
  }
  public enum EcQuality {
      L,
      M,
      Q,
      H
  }
  public enum CharacterMode {
      NUL,
      NUM,
      AN,
      B8,
      KANJI,
      STRUCTURE
  }
  [Flags]
  public enum Data {
    ENABLE,
    ECC_MODE,
    FORMAT,
    VERSION,
    TIMING,
    ALIGMENT,
    FINDER,
    NON_DATA
  }
  public enum CaseSensitive {
    NO = 0,
    YES = 1
  }
}
